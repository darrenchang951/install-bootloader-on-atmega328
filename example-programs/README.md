# Upload programs to `ATmega328P U`
## Using a RaspberryPi
### Prepare RaspberryPi
1. Enable SPI interface. `sudo raspi-config`, go to `Interfacing Options->SPI` and select `Enable`
2. Install required packages. `sudo apt-get update; sudo apt-get install arduino arduino-mk -y`
3. Install avrdude. 
```
wget http://project-downloads.drogon.net/gertboard/avrdude_5.10-4_armhf.deb;
sudo dpkg -i avrdude_5.10-4_armhf.deb;
sudo chmod 4755 /usr/bin/avrdude;
```

### Upload program
1. Connect to ATmega328P U to RaspberryPi's GPIOs as shown in the diagram below. *This breadboad is for `ledblink`*.
![Blink Diagram](ledblink/blink.png)
2. Check the connection between RaspberryPi and ATmega328P U. `avrdude -p m328p -c gpio`. You should get the following 
output `avrdude: AVR device initialized and ready to accept instructions`.
3. Go to one of the example programs (ledblink). `cd example-programs/ledblink`
4. Compile the program. `make`
5. Upload the program to ATmega328P U. `./upload.sh`
6. Now your LED light should start blinking. You can now disconnect your RaspberryPi. The light will continue to to 
blink whenever your ATmega328P U is powered on.  
 