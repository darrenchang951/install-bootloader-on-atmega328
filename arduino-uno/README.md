# Install bootloader on `ATmega328P U` with buildin 8MHz oscillator
## Requirements
### Hardware
- ATmega328P U (we will be programming this microcontroller)
- Arduino Uno (used to install bootloader on the ATmega328P U)
- Breadboard
- Jumper wires

### Software
- Arduino IDE 1.6.x (we use the older version because the hardware configuration seems to be incompatible with newer 
versions, such as 1.8.8). [Download here](https://www.arduino.cc/en/Main/OldSoftwareReleases)
- Hardware configuration [breadboard-1-6-x.zip](https://www.arduino.cc/en/uploads/Tutorial/breadboard-1-6-x.zip). This 
can be found at: <https://www.arduino.cc/en/Tutorial/ArduinoToBreadboard>  

## Steps
### Prepare your Arduino Uno
#### Prepare your IDE
1. Start Arduino IDE 1.6.x.
2. Connect your Arduino Uno to your computer.
3. On the IDE, go to `Tools->Board` and change the board to **Arduino/Genuino Uno**.
4. On the IDE, go to `Tools->Programmer` and change the Programmer to **AVRISP mkll**.
5. On the IDE, go to `Tools->Port` and choose your Arduino Uno connection.
6. On the IDE, go to `File->Examples->ArduinoISP` and open **ArduinoISP**.

#### Upload Sketch
1. Upload this Sketch to your Arduino Uno. `Sketch->Upload`

### Install bootloader on your ATmega328P U
#### Prepare your IDE
1. Start Arduino IDE 1.6.x.
2. Find your **Sketchbook location** in preferences. For example, mine is `/Users/darren/Documents/Arduino`
3. Unzip `breadboard-1-6-x.zip` to obtain a folder called `breadboard`
4. Create `hardware` folder inside your **Sketchbook location** if you don't have it.
5. Place `breadboard` folder inside the `hardware`. Now it should look like this 
`/Users/darren/Documents/Arduino/hardware/breadboard`
6. Restart Arduino IDE 1.6.x. *If you don't restart the IDE, the new board won't be listed*.
7. On the IDE, go to `Tools->Board` and change the board to **ATmega328 on a breadboard (8 MHz internal clock)**.
8. On the IDE, go to `Tools->Programmer` and change the Programmer to **Arduino as ISP**.
9. On the IDE, go to `Tools->Port` and choose your Arduino Uno connection.

#### Burn Bootloader to your ATmega328P U
1. Connect your Arduino to the computer
2. Connect your ATmega328P U to your Arduino as shown in the diagram.
![Minimal Circuilt bb](minimal_circuit_bb.png)
3. On the IDE, go to `Tools` and select **Burn Bootloader**.
##### Troubleshooting
If you get the below error while burning the bootloader, you need to modify `hardware/breadboard/avr/boards.txt` file.
Change the line `atmega328bb.bootloader.extended_fuses=0x05` to `atmega328bb.bootloader.extended_fuses=0xfd`, then try 
to burn bootloader again. 
```
***failed;  
avrdude: WARNING: invalid value for unused bits in fuse "efuse", should be set to 1 according to datasheet
This behaviour is deprecated and will result in an error in future version
You probably want to use 0xfd instead of 0x05 (double check with your datasheet first).

``` 